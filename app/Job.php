<?php

namespace app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes;

    protected $table = 'jobs';
    protected $fillable = [

        'title','job_type','wanted_at','salary','skills','employer','description','place_size',
        'place','english_lang','arabic_lang','mandarin_lang','education','currency','religion','country','gender'
    ];

    public function job_applies()
    {
        return $this->belongsToMany(User::class, 'job_applies');
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }
}
