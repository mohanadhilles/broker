<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

trait FileUploadTrait
{


    /**
     * File upload trait used in controllers to upload files
     */
    public function savePhoto(Request $request)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $user = Auth::user();
        $avatarName = $user->id . '_avatar' . time() . '.' . request()->photo->getClientOriginalExtension();

        $request->photo->storeAs('avatars', $avatarName);

        $user->photo = $avatarName;
        $user->save();
        return $user;

    }

    public function saveFiles(Request $request)
    {
        $request->validate([
            'cv' => 'required|max:2048',
        ]);
        $user = Auth::user();
        $cv = $user->id . '_avatar' . time() . '.' . request()->cv->getClientOriginalExtension();

        $request->cv->storeAs('avatars', $cv);

        $user->cv = $cv;
        $user->save();
        return $user;

    }

}
