<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{

    public function sendResponse($result , $message){
        $response = [
            'code'  => 200,
            'success' => true ,
            'data' => $result,
            'message' => $message
        ];
        return response()->json($response , 200);
    }

    public function sendError($error , $errorMessages = [] , $code = 404){
        $response = [
            'code'  => 404,
            'success' => false ,
            'message' => $error
        ];
        if (!empty($errorMessages)) {

            $response['date'] = $errorMessages;
        }
        return response()->json($response , $code);

    }

    public function getUser(){
        $auth = User::where('id', Auth::id())->with('nationality')
            ->with('country')->with('place_of_birth')->with('english_lang')->with('arabic_lang')
            ->with('mandarin_lang')->with('currency')->with('education')->with('completion')->with('religion')
            ->with('job_type')->get();

        if (!($auth)) {

            return $this->sendError(' not found ! ');
        }
        return $this->sendResponse($auth, ' read successfully');
    }

}
