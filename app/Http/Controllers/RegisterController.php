<?php
namespace App\Http\Controllers;

use Dotenv\Validator;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends BaseController
{

    public function register(Request $request)
    {
        $validator =    \Illuminate\Support\Facades\Validator::make($request->all(), [
            'role' => 'required',
            'email' => 'required|unique:users|email',
            'name'=> 'required',
            'password'=> 'required',
            'c_password'=> 'required|same:password',
        ] );

        if ($validator -> fails()) {
            return $this->sendError('error validation', $validator->errors());
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;

        return $this->sendResponse($success , 'User created successfully');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->phone,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
            return $this->sendResponse($token , ' Token successfully');
        } else {
            return $this->sendError('error validation');
        }
    }

}

