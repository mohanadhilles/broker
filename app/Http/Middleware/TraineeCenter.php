<?php

namespace App\Http\Middleware;

use Closure;

class TraineeCenter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth()->check() && $request->User()->role === 5) {

            return $next($request);


        }
        return redirect()->back();
    }
}
