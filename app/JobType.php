<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobType extends Model
{

    use SoftDeletes;

    protected $fillable = ['title'];
    protected $table = 'job_types';


    public function user()
    {
        return $this->hasMany(User::class);
    }

}
