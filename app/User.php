<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;


class User extends Authenticatable
{

    use Notifiable, SoftDeletes, HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'brand', 'role', 'whats_app', 'experience',
        'worked_in', 'place_of_birth', 'job_type', 'nationality', 'arabic_lang',
        'mandarin_lang', 'english_lang', 'currency', 'religion', 'skills',
        'phone', 'date_of_birth', 'photo', 'cv', 'available_at', 'date_join_training_center',
        'age', 'status', 'city', 'facebook', 'passport', 'passport_date_of_issue',
        'passport_date_of_exp', 'exp_visa', 'marital_status', 'no_of_child', 'height','completion',
        'weight', 'gender', 'bio', 'code', 'provider_id', 'salary', 'education', 'country'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['imgUrl'];


    public function getImgUrlAttribute()
    {
        if (strpos($this->photo, 'http') !== false) {
           return $this->photo;
        } else if ($this->photo){
            return env('APP_URL') . '/storage/avatars/' . $this->photo;
        } else {
            return env('APP_URL') . '/img5.png';

        }
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setDateFormata($input)
    {
        $this->attributes['passport_date_of_exp'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        $this->attributes['date_of_birth'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        $this->attributes['exp_visa'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        $this->attributes['passport_date_of_issue'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        $this->attributes['exp_visa'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        $this->attributes['exp_visa'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');

    }

    public function getEntryDateAttribute($input)
    {
        return Carbon::createFromFormat('Y-m-d', $input)
            ->format(config('app.date_format'));
    }


    public function worked_in()
    {
        return $this->belongsTo(Country::class, 'nationality');

    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }


    public function isAdmin()
    {
        $role = Role::all();
        return $role->id;
    }


    public static function boot()
    {
        parent::boot();


        static::creating(function ($model) {
            $random_code = null;
            while ($random_code === null) {
                $tmp_random_code = '#' . rand(10000000, 99999999);
                if (!self::where('code', $tmp_random_code)->count()) {
                    $random_code = $tmp_random_code;
                }
            }
            $model->code = $random_code;
        });
    }

    public function setCodeAttribute($input)
    {
        $this->attributes['code'] = $input ? $input : null;
    }

    public function setRoleIdAttribute($input)
    {
        $this->attributes['role'] = $input ? $input : null;
    }

    public function skills()
    {

        return $this->belongsToMany(Skill::class, 'users_skills');

    }

    public function place_of_birth()
    {
        return $this->belongsTo(Country::class, 'nationality');

    }

    public function nationality()
    {
        return $this->belongsTo(Country::class, 'nationality');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'nationality');
    }

    public function english_lang()
    {
        return $this->belongsTo(Language::class, 'english_lang');
    }

    public function arabic_lang()
    {
        return $this->belongsTo(Language::class, 'arabic_lang');
    }

    public function mandarin_lang()
    {
        return $this->belongsTo(Language::class, 'mandarin_lang');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency');
    }

    public function education()
    {
        return $this->belongsTo(Education::class, 'education');
    }

    public function completion()
    {
        return $this->belongsTo(Completion::class, 'completion');
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class, 'religion');
    }

    public function job_type()
    {
        return $this->belongsTo(JobType::class, 'job_types');
    }

    public function job_applies()
    {
        return $this->belongsToMany(Job::class, 'job_applies');
    }

    public function city()
    {

        return $this->belongsTo(City::class, 'city');
    }


}
