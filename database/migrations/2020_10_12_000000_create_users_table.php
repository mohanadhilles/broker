<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('brand')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('phone')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('photo')->nullable();
            $table->string('cv')->nullable();
            $table->date('available_at')->nullable();
            $table->integer('age')->nullable();
            $table->date('date_join_training_center')->nullable();
            $table->float('salary')->nullable();
            $table->string('facebook')->nullable();
            $table->string('passport')->nullable();
            $table->date('passport_date_of_issue')->nullable();
            $table->date('passport_date_of_exp')->nullable();
            $table->date('exp_visa')->nullable();
            $table->string('marital_status')->nullable();
            $table->integer('no_of_child')->nullable();
            $table->integer('height')->unsigned()->nullable();
            $table->integer('weight')->unsigned()->nullable();
            $table->integer('gender')->nullable();
            $table->enum('status',[0,1])->nullable();
            $table->text('bio')->nullable();
            $table->string('code')->nullable();
            $table->string('provider_id')->nullable();

            $table->string('whats_app')->nullable();
            $table->integer('experience')->nullable();
            $table->string('worked_in')->nullable();


            $table->integer('role')->nullable()->unsigned();
            $table->foreign('role')->references('id')->on('roles')->onDelete('cascade');

            $table->integer('place_of_birth')->nullable()->unsigned();
            $table->foreign('place_of_birth')->references('id')->on('countries')->onDelete('cascade');

            $table->integer('job_type')->nullable()->unsigned();
            $table->foreign('job_type')->references('id')->on('job_types')->onDelete('cascade');

            $table->integer('nationality')->nullable()->unsigned();
            $table->foreign('nationality')->references('id')->on('countries')->onDelete('cascade');


            $table->integer('english_lang')->nullable()->unsigned();
            $table->foreign('english_lang')->references('id')->on('languages')->onDelete('cascade');

            $table->integer('arabic_lang')->nullable()->unsigned();
            $table->foreign('arabic_lang')->references('id')->on('languages')->onDelete('cascade');

            $table->integer('mandarin_lang')->nullable()->unsigned();
            $table->foreign('mandarin_lang')->references('id')->on('languages')->onDelete('cascade');

            $table->integer('place')->nullable()->unsigned();
            $table->foreign('place')->references('id')->on('places')->onDelete('cascade');

            $table->integer('education')->nullable()->unsigned();
            $table->foreign('education')->references('id')->on('education')->onDelete('cascade');

            $table->integer('currency')->nullable()->unsigned();
            $table->foreign('currency')->references('id')->on('currencies')->onDelete('cascade');

            $table->integer('religion')->nullable()->unsigned();
            $table->foreign('religion')->references('id')->on('religions')->onDelete('cascade');

            $table->integer('country')->nullable()->unsigned();
            $table->foreign('country')->references('id')->on('countries')->onDelete('cascade');

            $table->integer('city')->nullable()->unsigned();
            $table->foreign('city')->references('id')->on('cities')->onDelete('cascade');

            $table->integer('completion')->nullable()->unsigned();
            $table->foreign('completion')->references('id')->on('completions')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
