<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->date('wanted_at')->nullable();
            $table->float('salary')->nullable();
            $table->integer('place_size')->nullable();
            $table->string('gender')->nullable();
            $table->string('contract')->nullable();

            $table->integer('employer')->nullable()->unsigned();
            $table->foreign('employer')->references('id')->on('users')->onDelete('cascade');


            $table->integer('job_type')->nullable()->unsigned();
            $table->foreign('job_type')->references('id')->on('job_types')->onDelete('cascade');

            $table->integer('english_lang')->nullable()->unsigned();
            $table->foreign('english_lang')->references('id')->on('languages')->onDelete('cascade');

            $table->integer('arabic_lang')->nullable()->unsigned();
            $table->foreign('arabic_lang')->references('id')->on('languages')->onDelete('cascade');

            $table->integer('mandarin_lang')->nullable()->unsigned();
            $table->foreign('mandarin_lang')->references('id')->on('languages')->onDelete('cascade');

            $table->integer('place')->nullable()->unsigned();
            $table->foreign('place')->references('id')->on('places')->onDelete('cascade');

            $table->integer('education')->nullable()->unsigned();
            $table->foreign('education')->references('id')->on('education')->onDelete('cascade');

            $table->integer('currency')->nullable()->unsigned();
            $table->foreign('currency')->references('id')->on('currencies')->onDelete('cascade');

            $table->integer('religion')->nullable()->unsigned();
            $table->foreign('religion')->references('id')->on('religions')->onDelete('cascade');

            $table->integer('country')->nullable()->unsigned();
            $table->foreign('country')->references('id')->on('countries')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
