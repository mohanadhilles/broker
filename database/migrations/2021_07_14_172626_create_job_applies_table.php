<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applies', function (Blueprint $table) {

            $table->integer('employer')->nullable()->unsigned();
            $table->foreign('employer')->references('id')->on('users')->onDelete('cascade');

            $table->integer('worker')->nullable()->unsigned();
            $table->foreign('worker')->references('id')->on('users')->onDelete('cascade');

            $table->integer('agency')->nullable()->unsigned();
            $table->foreign('agency')->references('id')->on('users')->onDelete('cascade');

            $table->integer('job')->nullable()->unsigned();
            $table->foreign('job')->references('id')->on('jobs')->onDelete('cascade');
            $table->enum('status',[0,1])->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applies');
    }
}
