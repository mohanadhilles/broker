<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $places = [

                ['id' => 1, 'title' => 'house',],
                ['id' => 2, 'title' => 'villa',],
                ['id' => 3, 'title' => 'apartment',],
                ['id' => 4, 'title' => 'palace',],

            ];


            $completions = [

                ['id' => 1, 'title' => 'Dark skin',],
                ['id' => 2, 'title' => 'Light skin',],
                ['id' => 3, 'title' => 'Europe',],
                ['id' => 4, 'title' => 'East Asia',],
                ['id' => 5, 'title' => 'Tanning response',],
                ['id' => 6, 'title' => 'Albinism',],


            ];

            $educations = [

                ['id' => 1, 'title' => 'primary',],
                ['id' => 2, 'title' => 'secondary',],
                ['id' => 3, 'title' => 'preparatory',],
                ['id' => 4, 'title' => 'Diploma',],

            ];

            $work_types = [

                ['id' => 1, 'title' => 'Baby Sitting',],
                ['id' => 2, 'title' => 'Cleaning',],
                ['id' => 3, 'title' => 'Cooking	',],
                ['id' => 4, 'title' => 'Washing',],
                ['id' => 5, 'title' => 'Caring of new born babies',],
                ['id' => 6, 'title' => 'Caring of disabled person',],
                ['id' => 7, 'title' => 'Household chore',],
                ['id' => 8, 'title' => 'Elder Care',],
            ];

            $religions = [

                ['id' => 1, 'title' => 'Christian',],
                ['id' => 2, 'title' => 'Hindu',],
                ['id' => 3, 'title' => 'Muslim',],
                ['id' => 4, 'title' => 'Chinese',],
                ['id' => 5, 'title' => 'jewish',],


            ];

            $langs = [

                ['id' => 1, 'title' => 'Fluent',],
                ['id' => 2, 'title' => 'Fire',],
                ['id' => 3, 'title' => 'Poor',],

            ];





            foreach ($places as $item) {
                \App\Place::create($item);
            }


            foreach ($langs as $item) {
                \App\Language::create($item);
            }


            foreach ($completions as $item) {
                \App\Completion::create($item);
            }

            foreach ($educations as $item) {
                \App\Education::create($item);
            }

            foreach ($work_types as $item) {
                \App\JobType::create($item);
            }
            foreach ($religions as $item) {
                \App\Religion::create($item);
            }



            factory(\App\Country::class, 243)->create();
            factory(\App\Currency::class, 155)->create();
        }

        $roles = [

            ['id' => 1, 'title' => 'Administrator',],
            ['id' => 2, 'title' => 'Worker',],
            ['id' => 3, 'title' => 'Employer',],
            ['id' => 4, 'title' => 'Agency',],

        ];

        foreach ($roles as $item) {
            \App\Role::create($item);
        }

//        $users = [
//
//            ['id' => 1, 'name' => 'mohanad', 'email' => 'admin@admin.com', 'password' => '$2y$10$LuHs8cUqHeGdYV9vJJv7h.UhzbT/M81ckTlQBsWMW/7h1z6EdRbTS', 'role' => 2, 'remember_token' => '',],
//            ['id' => 2, 'name' => 'abdalla', 'email' => 'abd@mery.com', 'password' => '$2y$10$LuHs8cUqHeGdYV9vJJv7h.UhzbT/M81ckTlQBsWMW/7h1z6EdRbTS', 'role' => 3, 'remember_token' => '',],
//            ['id' => 3, 'name' => 'admin', 'email' => 'admin@mery.com', 'password' => '$2y$10$LuHs8cUqHeGdYV9vJJv7h.UhzbT/M81ckTlQBsWMW/7h1z6EdRbTS', 'role' => 4, 'remember_token' => '',],
//        ];
//
//        foreach ($users as $item) {
//            \App\User::create($item);
//        }

        factory(\App\User::class, 243)->create();
        factory(\App\Skill::class, 100)->create();


    }
}
