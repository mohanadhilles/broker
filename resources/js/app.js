import Vue from 'vue'
import VueRouter from 'vue-router';
import store from './store';
import VueSweetalert2 from 'vue-sweetalert2';
import EventBus from './event-bus'

import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/dist/js/bootstrap.js';
// import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(VueRouter);
Vue.prototype.$bus = EventBus;

import App from './views/App'
import Login from './views/Login';
import Home from './views/Home';
import Profile from './views/Profile';
import JobFilter from './views/jobFilter';
import About from './views/About';
import WantMery from './views/WantMery';
import NeedMery from './views/NeedMery';
import MeryFilter from './views/MeryFilter';
import Register from './views/Register';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
          path: '/profile',
          name: 'profile',
          component: Profile,
            meta: { requiresAuth: true }
        },
        {
          path: '/jobs-filter',
          name: 'jobs_filter',
          component: JobFilter,
            meta: { requiresAuth: true }
        },
        {
          path: '/need-mery',
          name: 'need_mery',
          component: NeedMery
        },

        {
          path: '/mery-filter',
          name: 'mery_filter',
          component: MeryFilter
        },

        {
          path: '/i-want-mery',
          name: 'want_mery',
          component: WantMery
        },
        {
          path: '/about',
          name: 'about',
          component: About
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },

    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 200 }
    }

});

router.beforeEach((to, from, next) => {

    // check if the route requires authentication and user is not logged in
    if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
        // redirect to login page
        next({ name: 'login' })
        return
    }

    // if logged in redirect to dashboard
    if((to.path === '/login' || to.path === '/register') && store.state.isLoggedIn) {
        next({ name: 'profile' });
        return
    }

    next()
})


const app = new Vue({
    el: '#app',
    components: { App },
    router,
});