@extends('layouts.master')

@section('content')

    <header id="up">
        <div class="header-slider">
            <div class="carousel slide" data-ride="carousel" id="slider">
                <ol class="carousel-indicators  ">
                    <li class="active" data-slide-to="0" data-target="#slider"></li>
                    <li data-slide-to="1" data-target="#slider"></li>
                    <li data-slide-to="2" data-target="#slider"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item carousel-item--custom active">
                        <!--                <img alt="First slide" class="d-block w-100" src="img/showCase.png">-->
                        <div class="carousel-caption carousel-caption--custom img-slide">
                            <!-- Header caption -->
                            <section class="header-text">
                                <h1> mery </h1>
                                <h2>Care for all you love.</h2>
                                <p>Search for after-school sitters, nannies, senior care and more.</p>
                            </section>
                        </div>
                    </div>
                    <div class="carousel-item carousel-item--custom">
                        <!--                <img alt="Second slide" class="d-block w-100" src="img/showCase.png">-->
                        <div class="carousel-caption carousel-caption--custom img-slide">
                            <!-- Header caption -->
                            <section class="header-text">
                                <h1> mery </h1>
                                <h2>Care for all you love.</h2>
                                <p>Search for after-school sitters, nannies, senior care and more.</p>
                            </section>
                        </div>
                    </div>
                    <div class="carousel-item carousel-item--custom">
                        <!--                <img alt="Third slide" class="d-block w-100" src="img/showCase.png">-->
                        <div class="carousel-caption carousel-caption--custom img-slide">
                            <!-- Header caption -->
                            <section class="header-text">
                                <h1> mery </h1>
                                <h2>Care for all you love.</h2>
                                <p>Search for after-school sitters, nannies, senior care and more.</p>
                            </section>
                        </div>
                    </div>
                </div>

            </div>


            <!--========= Login / Sign up Form =========-->
            <section id="auth-section">
                <div class="container">
                    <div class="row pt-4">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <h3 class="auth-header pl-5">Welcome to mery</h3>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="d-flex flex-row-reverse auth-nav">
                                <a class="tab-link nav-link tab--custom currentTab" data-tab="signup-tab" href="#signup-tab">sign
                                    up</a>
                                <a class="tab-link nav-link tab--custom " data-tab="login-tab"
                                   href="#login-tab">login</a>
                            </div>
                        </div>
                    </div>

                    <!--Login Form-->

                    <div class=" form-sides tabContent" id="login-tab">
                        <div class="row">
                            <div class="col-lg-6 col-md-12  col-sm-12 pl-5">
                                <form method="POST" action="{{ route('login') }}"  class="login-form  text-center pr-3">
                                    @csrf
                                    <div class="form-group form-group--custom">
                                        <input aria-describedby="emailHelp" class="form-control"
                                               name="email" placeholder="Your email"
                                               type="email">
                                    </div>

                                    <div class="form-group form-group--custom">
                                        <input aria-describedby="emailHelp" class="form-control" name="password"
                                               placeholder="Password"
                                               type="password">
                                    </div>

                                    <div class="form-group form-group--custom">
                                        <input aria-describedby="emailHelp"
                                               class="form-control btn btn-danger--custom"
                                               name="submit" type="submit" value="Login">
                                    </div>
                                    <div class="form-group form-group--custom  last-form checkbox-wrapper">
                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <input checked class="custom-checkbox" id="checkbox-1" type="checkbox">
                                                <label class="checkmark" for="checkbox-1"></label>
                                                <span>Remember me</span>
                                            </div>
                                            <div class="col-6 text-right">
                                                <a class="forget-pass" href=""> forget password?</a>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 d-flex align-items-center justify-content-center flex-column"
                                 style="line-height: 1.5;">
                                <h5 class="text-center p-2">or<br>login with</h5>

                                <div class="social p-2">
                                    <a href="#"><i class="fab fa-facebook-f fa-2x"></i></a>
                                    <a href="#"> <i class="fab fa-twitter fa-2x"></i></a>
                                    <a href="#"><i class="fab fa-linkedin-in fa-2x"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--sign up form-->
                    <div class="tabContent current" id="signup-tab">
                        <div class="row">
                            <div class="col-lg-6 col-md-12  col-sm-12 pl-5">
                                <form method="POST" action="{{ route('register') }}" class="login-form  text-center pr-3">
                                    @csrf
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label check-wrapper" for="inlineRadio1">
                                            <input class="form-check-input form-check-input--custom"
                                                   id="inlineRadio11"
                                                   name="role" type="radio" value="2">
                                            <span class="check"></span>
                                            House Wroker </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label check-wrapper" for="inlineRadio2">
                                            <input class="form-check-input form-check-input--custom"
                                                   id="inlineRadio22"
                                                   name="role" type="radio" value="3">
                                            <span class="check"></span>
                                            Employer</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label check-wrapper" for="inlineRadio3">
                                            <input class="form-check-input form-check-input--custom"
                                                   id="inlineRadio33"
                                                   name="role" type="radio" value="4">
                                            <span class="check"></span>
                                            Agency</label>
                                    </div>

                                    <div class="form-group form-group--custom">
                                        <input aria-describedby="nameHelp" class="form-control"
                                               name="name" placeholder="Your name"
                                               type="text">
                                    </div>

                                    <div class="form-group form-group--custom">
                                        <input aria-describedby="emailHelp" class="form-control"
                                               name="email" placeholder="Your email"
                                               type="email">
                                    </div>

                                    <div class="form-group form-group--custom">
                                        <input aria-describedby="emailHelp" class="form-control" name="password"
                                               placeholder="Password"
                                               type="password">
                                    </div>

                                    <div class="form-group form-group--custom">
                                        <input aria-describedby="emailHelp" class="form-control" name="password_confirmation"
                                               placeholder="Password"
                                               type="password">
                                    </div>


                                    <div class="form-group form-group--custom mb-4">
                                        <input aria-describedby="emailHelp"
                                               class="form-control btn btn-danger--custom"
                                               name="submit" type="submit" value="Sign up">
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 d-flex align-items-center justify-content-center flex-column"
                                 style="line-height: 1.5;">
                                <h5 class="text-center p-2">or<br>sign up with</h5>

                                <div class="social p-2">
                                    <a href="#"><i class="fab fa-facebook-f fa-2x"></i></a>
                                    <a href="#"> <i class="fab fa-twitter fa-2x"></i></a>
                                    <a href="#"><i class="fab fa-linkedin-in fa-2x"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </header>

    <best></best>







    {{--<div class="container">--}}
        {{--<div class="row justify-content-center">--}}
            {{--<div class="col-md-8">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-header">{{ __('Register') }}</div>--}}

                    {{--<div class="card-body">--}}
                        {{--<form method="POST" action="{{ route('register') }}">--}}
                            {{--@csrf--}}

                            {{--<div class="form-group row">--}}
                                {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

                                    {{--@error('name')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@enderror--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group row">--}}
                                {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

                                    {{--@error('email')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@enderror--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group row">--}}
                                {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}
                                    {{--<input type="hidden"  name="role" value="1">--}}

                                    {{--@error('password')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@enderror--}}
                                {{--</div>--}}
                            {{--</div>--}}



                            {{--<div class="form-group row">--}}
                                {{--<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group row mb-0">--}}
                                {{--<div class="col-md-6 offset-md-4">--}}
                                    {{--<button type="submit" class="btn btn-primary">--}}
                                        {{--{{ __('Register') }}--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
