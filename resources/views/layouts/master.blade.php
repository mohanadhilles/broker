<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('mery/img/logo.png') }}">
    <title>Mery </title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('mery/css/style.css') }}">
    <link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" rel="stylesheet">

</head>
<body>
<div id="app">

    @include('layouts.nav')

    @yield('content')

    @include('layouts.footer')

</div>
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('mery/js/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('mery/js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('mery/js/toggle.js') }}"></script>

</body>


</html>
