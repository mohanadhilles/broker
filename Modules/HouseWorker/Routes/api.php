<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('worker')->group(function () {
    Route::get('/', 'WorkerController@getWorker')->name('worker');
    Route::get('count', 'WorkerController@getCount')->name('getCount');
    Route::get('prefer/{id}','WorkerController@preferToFriend')->name('preferToFriend');

    Route::get('country', 'WorkerController@getCountry')->name('getCountry');
    Route::get('language', 'WorkerController@getLanguage')->name('getLanguage');
    Route::get('education', 'WorkerController@getEducation')->name('getEducation');
    Route::get('completion', 'WorkerController@getCompletion')->name('getCompletion');
    Route::get('religion','WorkerController@getReligion')->name('getReligion');
    Route::get('marital', 'WorkerController@getMarital')->name('getMarital');
    Route::get('currency', 'WorkerController@getCurrency')->name('getCurrency');
    Route::get('job_type', 'WorkerController@getJobType')->name('getJobType');
    Route::get('skills','WorkerController@getSkills')->name('getSkills');
    Route::get('filter/{country?}/{city?}/{name?}/{brand?}', 'WorkerController@filterEmployer')->name('filterEmployer');

});

Route::group(['middleware' => ['auth:api'], 'prefix' => 'worker', 'as' => 'worker.'], function () {

    Route::post('store', 'WorkerController@store')->name('store');
    Route::get('{id}/edit', 'WorkerController@edit')->name('editProfile');
    Route::post('{id}/update', 'WorkerController@update')->name('updateProfile');
    Route::post('avatar','WorkerController@updatePhoto')->name('WorkerController');
    Route::post('file','WorkerController@updateCV')->name('cv');

    Route::get('agency/{id}','WorkerController@showAgency')->name('showAgency');
    Route::get('employer/{id}','WorkerController@showEmployer')->name('showEmployer');



});
