<?php

namespace Modules\HouseWorker\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Progress extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','worker','status'];
}
