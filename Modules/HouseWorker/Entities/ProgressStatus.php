<?php

namespace Modules\HouseWorker\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgressStatus extends Model
{
    use SoftDeletes;

    protected $fillable = ['title'];
    protected $table = 'progress_statues';

}
