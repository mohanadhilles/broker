<?php

namespace Modules\HouseWorker\Http\Controllers;
use Illuminate\Http\Request;
use App\Completion;
use App\Country;
use App\Currency;
use App\Education;
use App\Http\Controllers\BaseController;
use App\JobType;
use App\Language;
use App\Religion;
use App\Skill;
use App\User;
use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Support\Facades\Auth;
use Modules\HouseWorker\Http\Requests\ProfileRequest;

class WorkerController extends BaseController
{
    use FileUploadTrait;
    public function getSklls(){
        $skills =  Skill::all();

        if (!($skills)) {

            return $this->sendError(' not found ! ');
        }
        return $this->sendResponse($skills, ' read successfully');
    }

    public function update(Request $request,$id)
    {
        $profile = User::findOrFail($id);
        $profile->skills;
        $profile->update($request->all());

        $profile->skills()->sync(array_filter((array)$request->input('skills')));

        if (!($profile)) {

            return $this->sendError(' not found ! ');
        }
        return $this->sendResponse($profile, ' read successfully');
    }

    public function preferToFriend($id){

        $worker = User::where('role', 2)->find($id);
        if (!($worker)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($worker, ' read successfully');
    }

    public function updatePhoto(Request $request)
    {
        $request = $this->savePhoto($request);
        if (!($request)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($request, ' read successfully');
    }


    public function updateCV(Request $request)
    {
        $request = $this->saveFiles($request);
        if (!($request)) {


            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($request, ' read successfully');
    }




    public function getCount()
    {

        $counts = User::where('role', '2')->count();

        if (!($counts)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($counts, ' read successfully');

    }


    protected function User()
    {

        return $users = Auth::user()->getAuthIdentifier();
    }


    public function showEmployer($id)
    {
        $profile = User::where('role','3')->with('nationality')
            ->with('country')->with('place_of_birth')->with('english_lang')->with('arabic_lang')
            ->with('mandarin_lang')->with('currency')->with('education')->with('completion')->with('religion')
           ->findOrFail($id);
        if (!($profile)) {

            return $this->sendError(' not found ! ');
        }
        return $this->sendResponse($profile, ' read successfully');
    }

    public function showAgency($id)
    {
        $profile = User::where('role','4')->with('nationality')
            ->with('country')->with('place_of_birth')->with('english_lang')->with('arabic_lang')
            ->with('mandarin_lang')->with('currency')->with('education')->with('completion')->with('religion')
            ->findOrFail($id);
        if (!($profile)) {

            return $this->sendError(' not found ! ');
        }
        return $this->sendResponse($profile, ' read successfully');
    }

    public function filterEmployer($country = null, $city = null, $name = null, $brand = null)
    {

        $filler = User::where('role','3')->orWhere('role','4')->orWhere('country', 'like', "%{$country}%")
            ->orWhere('city', 'like', "%{$city}%")
            ->orWhere('name', 'like', "%{$name}%")
            ->orWhere('brand', 'like', "%{$brand}%")->with('nationality')
            ->with('country')->with('place_of_birth')->with('english_lang')->with('arabic_lang')
            ->with('mandarin_lang')->with('currency')->with('education')->with('completion')->with('religion')
            ->paginate(8);

        if (!($filler)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($filler, ' read successfully');
    }


    public function getWorker()
    {
        $worker = User::where('role', '2')->with('nationality')
            ->with('country')->with('place_of_birth')->with('english_lang')->with('arabic_lang')
            ->with('mandarin_lang')->with('currency')->with('education')->with('completion')->with('religion')->with('job_type')->paginate(8);

        if (!($worker)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($worker, ' read successfully');
    }


    public function getCountry()
    {

        $country = Country::get()->prepend(trans('mery.qa_please_select'), '');

        if (!($country)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($country, ' read successfully');
    }

    public function getJobType()
    {

        $jobtype = JobType::get()->prepend(trans('mery.qa_please_select'), '');

        if (!($jobtype)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($jobtype, ' read successfully');

    }

    public function getReligion()
    {
        $religion = Religion::get()->prepend(trans('mery.qa_please_select'), '');

        if (!($religion)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($religion, ' read successfully');


    }

    public function getSkills()
    {
        $skills = Skill::get()->prepend(trans('mery.qa_please_select'), '');

        if (!($skills)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($skills, ' read successfully');

    }

    public function getLanguage()
    {
        $language = Language::get()->prepend(trans('mery.qa_please_select'), '');

        if (!($language)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($language, ' read successfully');

    }

    public function getEducation()
    {

        $educations = Education::get()->prepend(trans('mery.qa_please_select'), '');
        if (!($educations)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($educations, ' read successfully');

    }

    public function getCurrency()
    {
        $currency = Currency::get()->prepend(trans('mery.qa_please_select'), '');

        if (!($currency)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($currency, ' read successfully');

    }

    public function getCompletion()
    {

        $completions = Completion::get()->prepend(trans('mery.qa_please_select'), '');
        if (!($completions)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($completions, ' read successfully');

    }

    public function getMarital()
    {

        $marital_status = ['1' => 'Single', '2' => 'Married', '3' => 'Sepad', '4' => 'Widowed'];
        if (!($marital_status)) {

            return $this->sendError(' not found ! ');
        }
        return $this->sendResponse($marital_status, ' read successfully');


    }

    public function getGender()
    {
        $gender = ['1' => 'male', '2' => 'female'];

        if (!($gender)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($gender, ' read successfully');
    }


    public function edit($id)
    {
        $user = User::findOrFail($id);

        if (!($user)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($user, ' read successfully');
    }


    public function getAuth()
    {
        return Auth::user()->getAuthIdentifier();
    }


}
