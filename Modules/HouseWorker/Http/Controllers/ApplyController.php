<?php

namespace Modules\HouseWorker\Http\Controllers;

use App\Http\Controllers\BaseController;
use app\Job;

class ApplyController extends BaseController
{

    public function allJobs()
    {

        $jobs = Job::paginate(5);

        if (!($jobs)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($jobs, ' read successfully');

    }

    public function filterJobs($country = null, $employer = null, $salary = null, $place = null, $title = null, $job_type = null, $gender = null)
    {

        $filler = Job::where('country', 'like', "%{$country}%")
            ->orWhere('employer', 'like', "%{$employer}%")
            ->orWhere('salary', 'like', "%{$salary}%")
            ->orWhere('place', 'like', "%{$place}%")
            ->orWhere('title', 'like', "%{$title}%")
            ->orWhere('job_type', 'like', "%{$job_type}%")
            ->orWhere('gender', 'like', "%{$gender}%")
            ->get();

        if (!($filler)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($filler, ' read successfully');
    }

    public function detailsJob($id)
    {
        $job = Job::findOrFail($id);
        if (!($job)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($job, ' read successfully');
    }

}
