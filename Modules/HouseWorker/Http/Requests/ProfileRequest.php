<?php

namespace Modules\HouseWorker\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class ProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'salary' => 'required',
            'name' => 'string',
            'email' => 'null|email',
            'brand' => 'null|string',
            'country'=> 'null',
            'nationality'=>'required',
            'english_lang'=>'required',
            'arabic_lang'=>'required',
            'mandarin_lang'=>'required',
            'role'=>'required',
            'skill_users'=>'exists:skill,id|null',
            'currency'=>'null',
            'education'=>'required',
            'completion'=>'null',
            'religion'=>'null',
            'job_types'=>'null',
            'phone'=>'null',
            'date_of_birth'=>'null',
            'photo'=>'required',
            'cv'=>'null',
            'available_at'=>'null',
            'age'=>'null',
            'status'=>'null',
            'facebook'=>'null',
            'passport'=>'null',
            'passport_date_of_issue'=>'required',
            'passport_date_of_exp'=>'required',
            'exp_visa'=>'null',
            'marital_status'=>'null',
            'no_of_child'=>'null',
            'height'=>'null',
            'weight'=>'null',
            'gender'=>'null',
            'bio'=>'null',
            'code'=>'null',
            'provider_id'=>'null',
            'city'=>'null',
            'whats_app'=>'null',
            'experience'=>'null|numeric',
            'worked_in'=>'required',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


}
