const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/mery/js/app.js', 'js/houseworker.js')
    .sass( __dirname + '/Resources/mery/sass/app.scss', 'css/houseworker.css');

if (mix.inProduction()) {
    mix.version();
}