<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('agency')->group(function () {
    Route::get('/', 'AgencyController@getِAgency')->name('agency');
    Route::get('count','AgencyController@getCount')->name('getCount');
    Route::get('filters/{name?}','AgencyController@filterByName')->name('filterByName');
    Route::get('filter/{country?}/{nationality?}/{name?}/{salary?}/','AgencyController@filterWorker')->name('filterWorker');


});

Route::group(['middleware' => ['auth:api', 'agency'], 'prefix' => 'agency', 'as' => 'agency.'], function () {
    Route::get('worker/{id}','AgencyController@showWorker')->name('showWorker');
    Route::get('{id}/edit', 'AgencyController@edit')->name('edit');
    Route::post('avatar','AgencyController@updatePhoto')->name('avatar');
    Route::post('update/{id}', 'AgencyController@update')->name('UpdateEmployer');
    Route::post('file','AgencyController@updateFile')->name('cv');

});
