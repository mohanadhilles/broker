@include('admin::partial.head')
@include('admin::partial.header')
@include('admin::partial.aside')
@include('admin::partial.subheader')

<!-- END: Subheader -->
<div class="m-content">
    @yield('admin')

</div>
</div>
</div>

<!-- end:: Body -->

@include('admin::partial.footer')
@include('admin::partial.script')
