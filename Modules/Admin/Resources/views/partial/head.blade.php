<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>Mery | Dashboard</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {


                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="{{asset("admin/vendors/perfect-scrollbar/css/perfect-scrollbar.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/tether/dist/css/tether.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-select/dist/css/bootstrap-select.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/select2/dist/css/select2.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/nouislider/distribute/nouislider.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/owl.carousel/dist/assets/owl.carousel.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/owl.carousel/dist/assets/owl.theme.default.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/ion-rangeslider/css/ion.rangeSlider.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/dropzone/dist/dropzone.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/summernote/dist/summernote.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/toastr/build/toastr.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/jstree/dist/themes/default/style.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/morris.js/morris.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/chartist/dist/chartist.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/sweetalert2/dist/sweetalert2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/socicon/css/socicon.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/vendors/line-awesome/css/line-awesome.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/vendors/flaticon/css/flaticon.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/vendors/metronic/css/styles.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/vendors/vendors/fontawesome5/css/all.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/assets/demo/base/style.bundle.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css")}}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{asset("admin/assets/demo/media/img/logo/favicon.ico")}}" />
</head>
