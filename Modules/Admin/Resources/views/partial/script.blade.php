<script src="{{asset("admin/vendors/jquery/dist/jquery.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/popper.js/dist/umd/popper.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap/dist/js/bootstrap.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js-cookie/src/js.cookie.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/moment/min/moment.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/tooltip.js/dist/umd/tooltip.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/perfect-scrollbar/dist/perfect-scrollbar.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/wnumb/wNumb.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/jquery.repeater/src/lib.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/jquery.repeater/src/jquery.input.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/jquery.repeater/src/repeater.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/jquery-form/dist/jquery.form.min.js")}}" type="˙text/javascript"></script>
<script src="{{asset("admin/vendors/block-ui/jquery.blockUI.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-switch/dist/js/bootstrap-switch.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-select/dist/js/bootstrap-select.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/select2/dist/js/select2.full.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/typeahead.js/dist/typeahead.bundle.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/handlebars/dist/handlebars.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/inputmask/dist/jquery.inputmask.bundle.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/nouislider/distribute/nouislider.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/owl.carousel/dist/owl.carousel.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/autosize/dist/autosize.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/clipboard/dist/clipboard.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/ion-rangeslider/js/ion.rangeSlider.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/dropzone/dist/dropzone.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/summernote/dist/summernote.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/markdown/lib/markdown.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-markdown/js/bootstrap-markdown.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/jquery-validation/dist/jquery.validate.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/jquery-validation/dist/additional-methods.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/forms/jquery-validation.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/bootstrap-notify/bootstrap-notify.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/toastr/build/toastr.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/jstree/dist/jstree.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/raphael/raphael.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/morris.js/morris.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/chartist/dist/chartist.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/chart.js/dist/Chart.bundle.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/charts/chart.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/vendors/jquery-idletimer/idle-timer.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/waypoints/lib/jquery.waypoints.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/counterup/jquery.counterup.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/es6-promise-polyfill/promise.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/sweetalert2/dist/sweetalert2.min.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/vendors/js/framework/components/plugins/base/sweetalert2.init.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/assets/demo/base/scripts.bundle.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js")}}" type="text/javascript"></script>
<script src="{{asset("admin/assets/app/js/dashboard.js")}}" type="text/javascript"></script>


@stack('scripts')
    <script>

    </script>

</body>
</html>
