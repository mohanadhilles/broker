@include('partial.head')
@include('partial.header')
@include('partial.aside')
@include('partial.subheader')


    <!-- END: Subheader -->
    <div class="m-content">
        @yield('admin')

    </div>
</div>
</div>

<!-- end:: Body -->



@include('partial.footer')
@include('partial.script')
