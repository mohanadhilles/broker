const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/mery/js/app.js', 'js/admin.js')
    .sass( __dirname + '/Resources/mery/sass/app.scss', 'css/admin.css');

if (mix.inProduction()) {
    mix.version();
}