<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::prefix('employer')->group(function () {
        Route::get('worker_profile/{id}','EmployerController@showWorkerProfile')->name('showWorkerProfile');
        Route::get('/', 'EmployerController@getEmployer')->name('employer');
        Route::get('count','EmployerController@getCount')->name('getCount');
        Route::get('filters/{name?}','EmployerController@filterByName')->name('filterByName');
    Route::get('filter/{country?}/{nationality?}/{name?}/{salary?}/','EmployerController@filterWorker')->name('filter');


});

Route::group(['middleware' => ['auth:api', 'employer'], 'prefix' => 'employer', 'as' => 'employer.'], function () {
    Route::get('{id}/edit', 'EmployerController@edit')->name('edit');
    Route::post('avatar','EmployerController@updatePhoto')->name('avatar');
    Route::post('file','EmployerController@updateFile')->name('cv');
    Route::post('update/{id}', 'EmployerController@update')->name('update');
    Route::get('worker/{id}','EmployerController@showWorker')->name('showWorker');

});

