<?php

namespace Modules\Employer\Http\Controllers;

use Modules\Employer\Http\Requests\JobRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Support\Facades\Auth;
use app\Job;

class PostJobController extends BaseController
{
    use FileUploadTrait;

    public function store(JobRequest $request)
    {
        $job = Job::create($request->all());
        $job->user = Auth::user()->getAuthIdentifier();
        if (!($job)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($job, ' read successfully');

    }

    public function edit($id)
    {
        $user = Job::findOrFail($id);
        if (!($user)) {
            return $this->sendError(' not found ! ');
        }
        return $this->sendResponse($user, ' read successfully');
    }


    public function update(JobRequest $request, $id)
    {
        $request = $this->saveFiles($request);
        $job = JobRequest::findOrFail($id);
        $job->user = Auth::user()->getAuthIdentifier();
        $job->update($request->all());

        if (!($job)) {

            return $this->sendError(' not found ! ');
        }

        return $this->sendResponse($job, ' read successfully');
    }


    public function massDestroy(Request $request)
    {

        if ($request->input('ids')) {
            $entries = Job::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    protected function User()
    {
        return $users = Auth::user()->getAuthIdentifier();

    }
}
