<?php

namespace Modules\Employer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required',
            'country'=> 'required',
            'nationality'=>'required',
            'english_lang'=>'required',
            'arabic_lang'=>'required',
            'mandarin_lang'=>'required',
            'role'=>'required',
            'skill.*' => 'exists:skill,id|null',
            'currency'=>'required',
            'education'=>'required',
            'completion'=>'required',
            'religion'=>'required',
            'job_types'=>'null',
            'phone'=>'required',
            'date_of_birth'=>'required',
            'photo'=>'null',
            'cv'=>'null',
            'available_at'=>'required',
            'age'=>'null',
            'status'=>'required',
            'facebook'=>'null',
            'passport'=>'null',
            'passport_date_of_issue'=>'null',
            'passport_date_of_exp'=>'null',
            'exp_visa'=>'null',
            'marital_status'=>'null',
            'no_of_child'=>'required',
            'height'=>'null',
            'weight'=>'null',
            'gender'=>'null',
            'bio'=>'null',
            'code'=>'null',
            'provider_id'=>'null',
            'city' => 'null',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
