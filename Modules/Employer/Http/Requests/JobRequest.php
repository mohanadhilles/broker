<?php

namespace Modules\Employer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'job_type' => 'required',
            'wanted_at' => 'required',
            'salary' => 'required',
            'skills.*' => 'exists:skills|required',
            'employer' => 'required',
            'description' => 'required',
            'place_size' => 'required',
            'place' => 'exist:places|required',
            'english_lang' => 'exist:languages|required',
            'arabic_lang' => 'exist:languages|required',
            'mandarin_lang' => 'exist:languages|required',
            'education' => 'exist:education|required',
            'currency' => 'exists:currency|required',
            'religion' => 'exist:religion|required',
            'country' => 'exist:country|required',
            'gender' => 'required',
            'contract'=>'required',

        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
