const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/mery/js/app.js', 'js/trainercenter.js')
    .sass( __dirname + '/Resources/mery/sass/app.scss', 'css/trainercenter.css');

if (mix.inProduction()) {
    mix.version();
}