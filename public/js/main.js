$(document).ready(function () {

    $('.auth-nav a').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('.auth-nav a').removeClass('currentTab');
        $('.tabContent').removeClass('current');

        $(this).addClass('currentTab');
        $("#" + tab_id).addClass('current');
    })


});


var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5d5ea7a177aa790be3303c72/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
})();

$('.profile-picture').click(function () {
    if($('section.resume').is(":visible")) {
        $('#Upload').click();
    }
});
